package com.example.imageupload;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.camerakit.CameraKitView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.UUID;

public class MainActivity extends AppCompatActivity  {
    private CameraKitView cameraKitView;
    private FloatingActionButton capture;
    private ImageView image;
    private LinearLayout linearLayout;
    private FloatingActionButton send;
    private TextView filenumber;
    private int numberofFiles;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        cameraKitView = findViewById(R.id.camera);
        capture = (FloatingActionButton) findViewById(R.id.capture);
        send = (FloatingActionButton) findViewById(R.id.send);
        linearLayout = (LinearLayout) findViewById(R.id.ImgContainer);
        Context context = getApplicationContext();
        filenumber=(TextView) findViewById(R.id.numberOfImage);
        filecount();
        capture.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraKitView.captureImage(new CameraKitView.ImageCallback() {
                    @Override
                    public void onImage(CameraKitView cameraKitView, final byte[] capturedImage) {
                        Bitmap bitmap = BitmapFactory.decodeByteArray(capturedImage, 0, capturedImage.length);
                        image = new ImageView(MainActivity.this);
                        image.setImageBitmap(bitmap);
                        image.setMaxWidth(30);
                        linearLayout.addView(image, 250, 300);
                        new SaveImage().execute(bitmap);
                        filenumber.setText( String.valueOf( ++numberofFiles));
                    }
                });
            }
        });
        send.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                linearLayout.removeAllViews();
                Intent intent = new Intent(MainActivity.this, Preview.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        cameraKitView.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
        cameraKitView.onResume();
        filecount();
    }

     void  filecount(){
            String path = "/data/user/0/com.example.imageupload/app_temp";
            File directory = new File(path);
            File[] files = directory.listFiles();
            if (files==null){
                numberofFiles=0;
            }else {
                numberofFiles=files.length;
            }
            TextView textView = (TextView) findViewById(R.id.numberOfImage);
            textView.setText( String.valueOf( numberofFiles));;
    }


    protected void onPause() {
        cameraKitView.onPause();
        super.onPause();
    }

    @Override
    protected void onStop() {
        cameraKitView.onStop();
        super.onStop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        cameraKitView.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private class SaveImage extends AsyncTask<Bitmap, String, String> {
        @Override
        protected String doInBackground(Bitmap... bitmaps) {
            Bitmap image = bitmaps[0];
            ContextWrapper cw = new ContextWrapper(getApplicationContext());
            File directory = cw.getDir("temp", Context.MODE_PRIVATE);
            File mypath = new File(directory, UUID.randomUUID().toString().replace("-", "") + ".png");
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(mypath);
                image.compress(Bitmap.CompressFormat.PNG, 100, fos);
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return directory.getAbsolutePath();
        }
    }
}

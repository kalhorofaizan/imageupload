package com.example.imageupload;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.firebase.FirebaseApp;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static com.example.imageupload.R.layout.support_simple_spinner_dropdown_item;

public class Preview extends AppCompatActivity {
    GridView gridImages;
    ImageAdapter imageAdapter;
    FloatingActionButton deletebtn, send;
    LinearLayout message;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReference();
    Spinner gendersp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview);


        gridImages = (GridView) findViewById(R.id.grid_images);
        message = (LinearLayout) findViewById(R.id.messageno);
        deletebtn = (FloatingActionButton) findViewById(R.id.delete);
        send = (FloatingActionButton) findViewById(R.id.send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Dialog dialog = new Dialog(Preview.this);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setCancelable(false);
                dialog.setContentView(R.layout.dialog_layout);
                EditText name = (EditText) dialog.findViewById(R.id.name);
                EditText email = (EditText) dialog.findViewById(R.id.email);
                EditText age = (EditText) dialog.findViewById(R.id.age);
                dialog.findViewById(R.id.dismiss).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                gendersp = (Spinner) dialog.findViewById(R.id.gender);
                ArrayAdapter<String> listgender= new ArrayAdapter<String>(Preview.this, support_simple_spinner_dropdown_item,getResources().getStringArray(R.array.gender));
                gendersp.setAdapter(listgender);
                dialog.findViewById(R.id.ok).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String nametext = name.getText().toString();
                        String emailtext=email.getText().toString();
                        String agetext=age.getText().toString();
                        String gendertext=gendersp.getSelectedItem().toString();
                        if (nametext.isEmpty() || emailtext.isEmpty() || agetext.isEmpty() ) {
                            Toast.makeText(Preview.this, "All field required required", Toast.LENGTH_SHORT).show();
                            return;
                        }
                        dialog.dismiss();
                        new SendData().execute(nametext,emailtext,agetext,gendertext);
                    }
                });
                dialog.show();
            }
        });

        findViewById(R.id.btnback).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Preview.super.onBackPressed();
            }
        });
        new GetData().execute();
        deletebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String path = "/data/user/0/com.example.imageupload/app_temp";
                File[] files = new File[imageAdapter.files.length - Collections.frequency(imageAdapter.selectedfiles, 1)];
                int j = 0;
                for (int i = 0; i < imageAdapter.files.length; i++) {
                    if (imageAdapter.selectedfiles.get(i) == 1) {

                        File file = new File(path + '/' + imageAdapter.files[i].getName());
                        file.delete();

                    } else {
                        files[j] = imageAdapter.files[i];
                        j++;
                    }
                }
                imageAdapter.files = files;
                if (files.length == 0) {
                    findViewById(R.id.grid_images).setVisibility(View.GONE);
                    message.setVisibility(View.VISIBLE);
                }
                imageAdapter.selectedfiles = new ArrayList<>(imageAdapter.files.length);

                ArrayList<Integer> selectedfiles = new ArrayList<Integer>();
                for (int i = 0; i < imageAdapter.files.length; i++) {
                    selectedfiles.add(0);
                }
                imageAdapter.selectedfiles = selectedfiles;
                deletebtn.setVisibility(View.GONE);
                imageAdapter.notifyDataSetChanged();
            }
        });

        gridImages.setOnItemClickListener(new GridView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ImageView image = (ImageView) view.findViewById(R.id.pic);
                ImageView check = (ImageView) view.findViewById(R.id.checkImage);
                if (imageAdapter.selectedfiles.get(position) != 1) {
                    imageAdapter.selectedfiles.set(position, 1);
                    check.setVisibility(View.VISIBLE);
                    image.setPadding(20, 20, 20, 20);
                } else {
                    imageAdapter.selectedfiles.set(position, 0);
                    check.setVisibility(View.GONE);
                    image.setPadding(0, 0, 0, 0);
                }
                if (imageAdapter.selectedfiles.contains(1)) {
                    deletebtn.setVisibility(View.VISIBLE);
                } else {
                    deletebtn.setVisibility(View.INVISIBLE);
                }
                imageAdapter.notifyDataSetChanged();
                if (imageAdapter.files.length == 0) {

                }
            }
        });
    }

    private class GetData extends AsyncTask<Void, Void, Integer> {
        private ProgressDialog Dialog = new ProgressDialog(Preview.this);

        @Override
        protected void onPreExecute() {
            try {
                Dialog.setMessage("Loading Images");
                Dialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onPostExecute(Integer result) {
            Dialog.dismiss();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            String path = "/data/user/0/com.example.imageupload/app_temp";
            File directory = new File(path);
            File[] files = directory.listFiles();
            ArrayList<Integer> selectedfiles = new ArrayList<Integer>();
            int i;
            if (files == null) {
                findViewById(R.id.grid_images).setVisibility(View.GONE);
                message.setVisibility(View.VISIBLE);
                return 0;
            }
            for (i = 0; i < files.length; i++) {
                selectedfiles.add(0);
            }
            imageAdapter = new ImageAdapter(getApplicationContext(), files, selectedfiles);
            gridImages.setAdapter(imageAdapter);
            return null;
        }
    }

    private class SendData extends AsyncTask<String, Void, Void> {
        private StorageReference storageRef;
        private FirebaseApp app;
        private FirebaseStorage storage;
        private ProgressDialog Dialog = new ProgressDialog(Preview.this);
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        @Override
        protected void onPreExecute() {
            try {
                Dialog.setMessage("Loading Images");
                Dialog.show();
                app = FirebaseApp.getInstance();
                storage = FirebaseStorage.getInstance(app);
                storageRef = storage.getReference();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        @Override
        protected void onPostExecute(Void result) {
            Dialog.dismiss();
        }

        @Override
        protected Void doInBackground(String... strings) {
            Map<String,Object> imagede= new HashMap<>();
            imagede.put("name",strings[0]);
            imagede.put("email",strings[1]);
            imagede.put("age",strings[2]);
            imagede.put("gender",strings[3]);

            db.collection("ImageDetail").add(imagede).addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                @Override
                public void onSuccess(DocumentReference documentReference) {
                    for (int i = 0; i < imageAdapter.files.length; i++) {
                        StorageReference ref = storageRef.child("images/" + documentReference.getId() + "/" + UUID.randomUUID().toString());
                        ref.putFile(Uri.fromFile(imageAdapter.files[i]));
                    }
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("FireBase", "onFailure: "+e );
                }
            });
                       return null;
        }
    }
}

class ImageAdapter extends BaseAdapter {
    Context context;
    File[] files;
    LayoutInflater inflater;
    ArrayList<Integer> selectedfiles;

    public ImageAdapter(Context applicationContext, File[] files, ArrayList<Integer> selectedfiles) {
        this.context = applicationContext;
        this.files = files;
        this.selectedfiles = selectedfiles;
        inflater = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return files.length;
    }

    @Override
    public Object getItem(int position) {
        try {
            Bitmap bitmap = BitmapFactory.decodeStream(new FileInputStream(files[position]));
            ImageView image = new ImageView(context);
            image.setMaxWidth(200);
            image.setMaxHeight(300);
            image.setImageBitmap(bitmap);
            return image;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Bitmap bitmap = null;
        try {
            bitmap = BitmapFactory.decodeStream(new FileInputStream(files[position]));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        convertView = inflater.inflate(R.layout.image_layout, null);
        ImageView pic = (ImageView) convertView.findViewById(R.id.pic);
        pic.setImageBitmap(bitmap);
        if (selectedfiles.get(position) == 1) {
            pic.setPadding(10, 10, 10, 10);
            convertView.findViewById(R.id.checkImage).setVisibility(View.VISIBLE);
        }
        return convertView;
    }
}